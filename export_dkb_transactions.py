#!/usr/bin/env python3
import sys
from getpass import getpass
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

DKB_DATE_FORMAT = "%d.%m.%Y"
FIREFLY_III_DATE_FORMAT = "%Y-%m-%d"

SETTINGS_FILENAME = ".export_dkb_transactions_settings.txt"

try:
    from dkb_robo import DKBRobo
    import requests
except ModuleNotFoundError:
    print("run 'pip install dkb-robo requests' to install necessary deps")
    sys.exit(1)

class FireflyConnection:
    def __init__(self, url, personal_token):
        self.url = url
        self.headers = {
            "Authorization": f"Bearer {personal_token}",
            "Accept": "application/json",
        }

    def get_start_date_for_account(self, account_id):
        response = requests.get(
            f"{self.url}/api/v1/accounts/{account_id}/transactions?limit=1",
            headers=self.headers,
        )
        response.raise_for_status()
        transactions = response.json()['data']
        if transactions:
            return datetime.fromisoformat(
                transactions[0]['attributes']['transactions'][0]['date'],
            )
        return datetime.now() - relativedelta(years=1)

    def get_accounts(self):
        response = requests.get(f"{self.url}/api/v1/accounts?type=asset", headers=self.headers)
        response.raise_for_status()
        return {
            account['attributes']['name']: {
                **account,
                "start_date": self.get_start_date_for_account(account["id"]),
            }
            for account in response.json()['data']
        }


    @staticmethod
    def calculate_opening_balance(current_balance, transactions):
        """
        Calculate the opening balance by applying transactions in reverse.
        :param current_balance: The current balance of the account.
        :param transactions: A list of transactions from the last year.
        :return: The calculated opening balance.
        """
        opening_balance = float(current_balance)
        for transaction in transactions:
            # Assuming transaction amounts are positive for deposits and negative for withdrawals.
            opening_balance -= float(transaction["amount"])
        return opening_balance


    def create_account(self, account_name, current_balance, transactions, account_type='asset'):
        one_year_ago = datetime.now() - relativedelta(years=1)
        opening_balance_date = one_year_ago.strftime(FIREFLY_III_DATE_FORMAT)
        opening_balance = self.calculate_opening_balance(current_balance, transactions)

        data = {
            "name": account_name,
            "type": account_type,
            "opening_balance": opening_balance,
            "opening_balance_date": opening_balance_date,
            "currency_code": "EUR",
            "account_role": "defaultAsset",
        }
        response = requests.post(
            f"{self.url}/api/v1/accounts",
            json=data,
            headers=self.headers,
        )
        return response.json()


    def insert_transaction(self, account_id, transaction):
        try:
            response = requests.post(
                f"{self.url}/api/v1/transactions",
                json={"error_if_duplicate_hash": True, "transactions": [transaction]},
                headers=self.headers,
            )
            response.raise_for_status()
            print(f"Inserted {transaction['type']} {transaction['date']} {transaction['amount']}: {transaction['description']}")
        except requests.exceptions.HTTPError as http_err:
            try:
                error_details = response.json()
                if "Duplicate" in error_details.get("message", ""):
                    print(f"Duplicate transaction not inserted: {transaction['type']} {transaction['date']} {transaction['amount']}: {transaction['description']}")
                else:
                    print(f"HTTP error occurred: {http_err} - {error_details}")
                    raise
            except ValueError:
                print(f"HTTP error occurred: {http_err}")
                raise


if __name__ == "__main__":
    try:
        with open(SETTINGS_FILENAME) as settings_file:
            user_name = settings_file.readline().strip()
            firefly_url = settings_file.readline().strip()
            personal_token = settings_file.readline().strip()
            if not personal_token:
                raise EOFError
    except Exception:
        user_name = input("dkb username: ")
        password = getpass("dkb password: ")
        firefly_url = input("firefly url: ")
        personal_token = input("firefly personal token: ")

        with open(SETTINGS_FILENAME, 'w') as settings_file:
            settings_file.write(user_name + "\n")
            settings_file.write(firefly_url + "\n")
            settings_file.write(personal_token)
    else:
        print(f"dkb username: '{user_name}'")
        print(f"firefly url: '{firefly_url}'")
        print(f"personal_token: '{personal_token}'")
        password = getpass("dkb password: ")

    firefly = FireflyConnection(firefly_url, personal_token)

    end_date = datetime.now() - relativedelta(days=1)

    with DKBRobo(dkb_user=user_name, dkb_password=password) as dkb:
        dkb_accounts = [
            account
            for account in dkb.account_dic.values()
            if account["type"] == "account"
        ]
        firefly_accounts = firefly.get_accounts()
        created_firefly_account = False
        # make sure all accounts exist in firefly
        for dkb_account in dkb_accounts:
            account_name = dkb_account["iban"]
            if account_name not in firefly_accounts:
                start_date = datetime.now() - relativedelta(years=1)
                transactions = dkb.get_transactions(
                    dkb_account["transactions"],
                    dkb_account["type"],
                    start_date.strftime(DKB_DATE_FORMAT),
                    end_date.strftime(DKB_DATE_FORMAT),
                )

                firefly_account = firefly.create_account(
                    account_name,
                    dkb_account["amount"],
                    transactions,
                )
                created_firefly_account = True
                firefly_account_id = firefly_account['data']['id']

        if created_firefly_account:
            firefly_accounts = firefly.get_accounts()
        for dkb_account in dkb_accounts:
            account_name = dkb_account["iban"]
            firefly_account = firefly_accounts[account_name]
            firefly_account_id = firefly_account['id']

            for transaction in dkb.get_transactions(
                dkb_account["transactions"],
                dkb_account["type"],
                firefly_account["start_date"].strftime(DKB_DATE_FORMAT),
                end_date.strftime(DKB_DATE_FORMAT),
            ):
                if transaction["amount"] == 0:
                    continue
                description = transaction.get(
                    "text",
                    transaction.get("reasonforpayment", "unknown"),
                )
                if "peeraccount" in transaction and transaction["peeraccount"] in firefly_accounts:
                    # only include positive transfers to prevent double booking
                    if transaction["amount"] > 0:
                        firefly.insert_transaction(
                            firefly_account_id,
                            {
                                "type": "transfer",
                                "date": transaction["date"],
                                "amount": transaction["amount"],
                                "description": description,
                                "source_id": firefly_accounts[transaction["peeraccount"]]["id"],
                                "destination_id": firefly_account_id,
                            },
                        )
                    continue
                firefly.insert_transaction(
                    firefly_account_id,
                    {
                        "type": "withdrawal",
                        "date": transaction["date"],
                        "amount": -transaction["amount"],
                        "description": description,
                        "source_id": firefly_account_id,
                    } if transaction["amount"] < 0 else {
                        "type": "deposit",
                        "date": transaction["date"],
                        "amount": transaction["amount"],
                        "description": description,
                        "destination_id": firefly_account_id,
                    },
                )
